const { Gio } = imports.gi;

export function loadContents(filePath) {
    let file = Gio.File.new_for_path(filePath);
    let [success, contents] = file.load_contents(null);

    if (success) {
        return imports.byteArray.toString(contents);
    }

    return null;
}