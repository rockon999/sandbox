const BINARY_NAME = 'gjs';

const { Gio } = imports.gi;

export class GJSWrapper {
    constructor() {
        this._argv = [BINARY_NAME];
    }

    _saveTempScript(content) {
        const [file] = Gio.File.new_tmp(null);
        const [] = file.replace_contents(content, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);

        return file;
    }

    tempScript(contents) {
        const tmpFile = this._saveTempScript(contents);
        this._argv.push(tmpFile.get_path());

        return this;
    }

    code(code) {
        this._argv.push('-c');
        this._argv.push(code);

        return this;
    }

    filename(filename) {
        this._argv.push(filename);

        return this;
    }

    toARGV() {
        return Array.from(this._argv);
    }
};