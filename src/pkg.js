pkg.initGettext();
pkg.initFormat();
pkg.require({
    'Gio': '2.0',
    'Gtk': '3.0',
    'GtkSource': '4'
});
